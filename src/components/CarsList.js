import { useContext } from "react";
import { CarsContext } from "../store/cars-context";
import Car from "./Car";

const CarsList = () => {
    const { setCars, cars } = useContext(CarsContext);

    const fetchCarsHandler = async () => {
        try {
            const response = await fetch("https://myfakeapi.com/api/cars");
            const data = await response.json();
            setCars(data.cars);
        } catch(e) {
            console.log(e);
        }
    }

    return (
        <div>
            <button onClick={fetchCarsHandler}>Fetch cars</button>
            {cars.map(car => <Car key={car.id} model={car.car_model} car={car.car} year={car.car_model_year} />)}
        </div>
    )
}

export default CarsList;