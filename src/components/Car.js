const Car = (props) => {
    return (
      <div>
        <h3>Info: </h3>
        <p>Car: {props.car}</p>
        <p>Model: {props.model}</p>
        <p>Year: {props.year}</p>
      </div>
    );
  };
  
  export default Car;
  