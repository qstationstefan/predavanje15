import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import CarsContextProvider from './store/cars-context';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <CarsContextProvider>
      <App />
    </CarsContextProvider>
  </React.StrictMode>
);