import { useContext } from "react";
import CarsList from "./components/CarsList";
import { CarsContext } from "./store/cars-context";

const App = () => {
  const { totalPrice } = useContext(CarsContext);

  return (
    <div>
      <p>Total price: {totalPrice}</p>
      <CarsList />
    </div>
  );
}

export default App;
