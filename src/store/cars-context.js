import React, { useCallback, useEffect, useState } from "react";

export const CarsContext = React.createContext([]);

const CarsContextProvider = (props) => {
    const [cars, setCars] = useState([]);
    const [totalPrice, setTotalPrice] = useState(0);

    const caclculateSum = useCallback(() => {
        const sum = cars.reduce((prevValue, currentValue) => prevValue + +currentValue.price.substring(1), 0);
        return sum;
    }, [cars]);

    useEffect(() => {
        const sum = caclculateSum();
        setTotalPrice(sum);
    }, [caclculateSum])

    return (
        <CarsContext.Provider value={
            {
                cars,
                setCars,
                totalPrice
            }
        }>
            {props.children}
        </CarsContext.Provider>
    )
}

export default CarsContextProvider;

